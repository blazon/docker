FROM php:cli

RUN apt-get update \
    && apt-get install -y git build-essential mariadb-client unzip zip \
    libbz2-dev libcurl4-openssl-dev libssl-dev libicu-dev libxml2-dev libzip-dev wget \
    nano unixodbc unixodbc-dev

RUN docker-php-ext-install mysqli pdo_mysql bz2 intl \
    opcache soap sockets zip bcmath

RUN mkdir -p /usr/src/php/ext/xdebug \
    && curl -fsSL https://pecl.php.net/get/xdebug | tar xvz -C "/usr/src/php/ext/xdebug" --strip 1 \
    && docker-php-ext-install xdebug \
    && rm /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
